# frozen_string_literal: true

module Headway
  module Config
    module_function

    def tracking_label
      return ENV.fetch('HEADWAY_TRACKING_LABEL') if ENV.key?('HEADWAY_TRACKING_LABEL')
    end

    def report_format
      ENV.fetch('HEADWAY_REPORT_FORMAT') { 'MILESTONE_MR_ISSUE_TOTAL' }
    end

    def epic_reference
      ENV.fetch('HEADWAY_EPIC_REFERENCE')
    end

    def epic_id
      ENV.fetch('HEADWAY_EPIC_ID')
    end

    def dry_run?
      ENV.key?('HEADWAY_DRY_RUN')
    end

    def configuration_details
      if !tracking_label.nil?
        <<~HTML
          <small>
            Use ~"#{tracking_label}" on MRs and issues to add them to this list.<br>
            Generated at <code>#{Time.now.utc}</code> by #{ENV['CI_JOB_URL']}.
          </small>
        HTML
      elsif !epic_id.nil?
        <<~HTML
          <small>
            Use epic #{epic_reference} on issues to add them to this list.<br>
            Generated at <code>#{Time.now.utc}</code> by #{ENV['CI_JOB_URL']}.
          </small>
        HTML
      end
    end
  end
end
