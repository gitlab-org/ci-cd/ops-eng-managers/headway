# frozen_string_literal: true

module Headway
  class IssueReportItem
    attr_reader :issue

    def initialize(issue)
      @issue = issue
    end

    def emoji
      ':arrows_counterclockwise:'
    end

    def title
      "#{reference}+"
    end

    def dri; end

    def reference
      issue.references.full
    end

    def milestone
      "%#{issue.milestone&.title}" if issue.milestone
    end

    def impact; end

    def to_row
      values = [
        emoji,
        title,
        reference,
        milestone,
        dri,
        impact
      ].join(' | ')

      "| #{values} |"
    end
  end
end
