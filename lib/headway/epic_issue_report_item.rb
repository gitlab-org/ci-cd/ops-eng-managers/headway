# frozen_string_literal: true

module Headway
  class EpicIssueReportItem
    attr_reader :issue

    def initialize(issue)
      @issue = issue
    end

    def emoji
      case issue.state
      when 'opened'
        ':hourglass_flowing_sand:'
      when 'closed'
        ':x:'
      else
        ':information_source:'
      end
    end

    def title
      "#{reference}+"
    end

    def dri; end

    def reference
      issue.references.full
    end

    def milestone
      "%#{issue.milestone&.title}" if issue.milestone
    end

    def impact; end

    def to_row
      values = [
        emoji,
        title,
        reference,
        milestone,
        dri,
        impact
      ].join(' | ')

      "| #{values} |"
    end
  end
end
