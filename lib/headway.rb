# frozen_string_literal: true

require 'logger'
require 'gitlab'

require_relative 'headway/issue_report_item'
require_relative 'headway/merge_request_report_item'
require_relative 'headway/config'
require_relative 'headway/processor'
require_relative 'headway/epic_issue_report_item'

module Headway
  class Error < StandardError; end
end
