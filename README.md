# Headway

A tool for tracking progress for projects

Heavily inspired by https://gitlab.com/nolith/review-tanuki

## Usage

### Setup

Get yourself an [API private token](https://gitlab.com/-/profile/personal_access_tokens) with the "api" scope.
You can now run it using one of the following methods:

1. Locally (e.g. on a Raspberry Pi or a laptop)
1. On a server
2. On GitLab CI, using CI schedules

In all cases, it comes down to running the following command:

```
env HEADWAY_EPIC_REFERENCE='group&iid' GITLAB_API_PRIVATE_TOKEN='...' HEADWAY_REPORT_FORMAT='{Insert report format type here}' GITLAB_API_ENDPOINT='https://gitlab.com/api/v4' bundle exec ./bin/headway

```

**NOTE: ** Remember that each configuration option below may dictate a slight difference in the command above to include/exclude options based on format type requested.

### Configuration options

- `HEADWAY_EPIC_REFERENCE` - the epic that will be updated
- `HEADWAY_DRY_RUN` - skips epic update when present
- `HEADWAY_TRACKING_LABEL` - the label that will be used for filtering merge requests and issues
- `HEADWAY_REPORT_FORMAT` - this value can be either `MILESTONE_MR_ISSUE_TOTAL` (default if not specified), `MILESTONE_TOTAL` or `EPIC_ISSUES_BY_MILESTONE_TOTAL`
   - `MILESTONE_MR_ISSUE_TOTAL` - Will group MRs and Issues together in separate sections (with counts) and roll up on a per milestone basis in each section.
   - `MILESTONE_TOTAL` - Will combine MRs and Issues together and roll up on  a per milestone basis.
   - `EPIC_ISSUES_BY_MILESTONE_TOTAL` - Will group Issues together in separate sections (with counts) and roll up on a per milestone basis in each section. (This format is ideal of sub-epics of main epics)
- `HEADWAY_EPIC_ID` - the epic that is linked to issues - **NOTE: ** If this value is provided, `HEADWAY_TRACKING_LABEL` should not be provided in command.

### Sample Rubocop commands to test for valid syntax locally 

- `bundle exec rubocop lib/headway/processor.rb`
- `bundle exec rubocop lib/headway/config.rb`
- etc.